package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

public class ReallyDough implements Dough {
    public String toString() {
        return "ReallyDough?";
    }
}
