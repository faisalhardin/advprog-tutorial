package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class RendangSauce implements Sauce {

    public String toString() {
        return "Traditional Rendang Sauce";
    }
}
