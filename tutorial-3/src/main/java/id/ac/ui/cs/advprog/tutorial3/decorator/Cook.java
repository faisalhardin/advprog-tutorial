package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class Cook {
    public static void main(String[] args) {

        Food thickBunBurger = BreadProducer.THICK_BUN.createBreadToBeFilled();
        thickBunBurger = FillingDecorator.BEEF_MEAT.addFillingToBread(
                thickBunBurger);
        thickBunBurger = FillingDecorator.CHEESE.addFillingToBread(
                thickBunBurger);
        thickBunBurger = FillingDecorator.CUCUMBER.addFillingToBread(
                thickBunBurger);
        thickBunBurger = FillingDecorator.LETTUCE.addFillingToBread(
                thickBunBurger);
        thickBunBurger = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                thickBunBurger);
        System.out.println(thickBunBurger.getDescription() + " = " + thickBunBurger.cost());

        Food thinBunBurger = BreadProducer.THIN_BUN.createBreadToBeFilled();
        thinBunBurger = FillingDecorator.TOMATO.addFillingToBread(
                thinBunBurger);
        thinBunBurger = FillingDecorator.LETTUCE.addFillingToBread(
                thinBunBurger);
        thinBunBurger = FillingDecorator.CUCUMBER.addFillingToBread(
                thinBunBurger);
        System.out.println(thinBunBurger.getDescription() + " = " + thinBunBurger.cost());

        Food crustySandwich = BreadProducer.CRUSTY_SANDWICH.createBreadToBeFilled();
        crustySandwich = FillingDecorator.BEEF_MEAT.addFillingToBread(
                crustySandwich);
        crustySandwich = FillingDecorator.CHICKEN_MEAT.addFillingToBread(
                crustySandwich);
        crustySandwich = FillingDecorator.CHILI_SAUCE.addFillingToBread(
                crustySandwich);
        System.out.println(crustySandwich.getDescription() + " = " + crustySandwich.cost());

        Food noCrustSandwich = BreadProducer.NO_CRUST_SANDWICH.createBreadToBeFilled();
        noCrustSandwich = FillingDecorator.BEEF_MEAT.addFillingToBread(
                noCrustSandwich);
        noCrustSandwich = FillingDecorator.CHEESE.addFillingToBread(
                noCrustSandwich);
        noCrustSandwich = FillingDecorator.TOMATO.addFillingToBread(
                noCrustSandwich);
        noCrustSandwich = FillingDecorator.LETTUCE.addFillingToBread(
                noCrustSandwich);
        System.out.println(noCrustSandwich.getDescription() + " = " + noCrustSandwich.cost());

    }
}