package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    public Cto(String name, double salary) {
        //TODO Implement
        if (salary < 100000.00) {
            throw new IllegalArgumentException("CTO Salary must not lower than 100000.00");
        }
        super.name = name;
        super.salary = salary;
        this.role = "CTO";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return salary;
    }
}
