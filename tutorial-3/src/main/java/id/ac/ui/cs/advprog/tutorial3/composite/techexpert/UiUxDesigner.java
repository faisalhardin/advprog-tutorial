package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class UiUxDesigner extends Employees {
    //TODO Implement


    public UiUxDesigner(String name, double salary) {
        if (salary < 90000.00) {
            throw new IllegalArgumentException("CTO Salary must not lower than 90000.00");
        }
        super.name = name;
        super.salary = salary;
        super.role = "UI/UX Designer";
    }

    public double getSalary() {
        return salary;
    }
}
