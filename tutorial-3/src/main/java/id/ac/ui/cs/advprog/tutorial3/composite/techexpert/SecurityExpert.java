package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class SecurityExpert extends Employees {
    //TODO Implement


    public SecurityExpert(String name, double salary) {
        if (salary < 70000.00) {
            throw new IllegalArgumentException("CTO Salary must not lower than 70000.00");
        }
        super.name = name;
        super.salary = salary;
        super.role = "Security Expert";
    }

    public double getSalary() {
        return salary;
    }
}
