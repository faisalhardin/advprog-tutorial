package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class FrontendProgrammer extends Employees {
    //TODO Implement


    public FrontendProgrammer(String name, double salary) {
        if (salary < 30000.00) {
            throw new IllegalArgumentException("CTO Salary must not lower than 30000.00");
        }
        super.name = name;
        super.salary = salary;
        super.role = "Front End Programmer";
    }

    public double getSalary() {
        return salary;
    }
}
