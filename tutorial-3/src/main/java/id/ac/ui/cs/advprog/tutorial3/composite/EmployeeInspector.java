package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;

import java.util.List;

public class EmployeeInspector {

    public static void main(String[] args) {
        Company company = new Company();

        Ceo dostoyevsky = new Ceo("Dostoyevsky", 500000.00);
        company.addEmployee(dostoyevsky);

        Cto kierkiegaard = new Cto("Kierkiegaard", 320000.00);
        company.addEmployee(kierkiegaard);

        BackendProgrammer frankl = new BackendProgrammer("Frankl", 94000.00);
        company.addEmployee(frankl);

        List<Employees> allEmployees = company.getAllEmployees();
        allEmployees.stream().forEach(employee -> System.out.println(employee.getName()
                + " " + employee.getRole() + " " + employee.getSalary()));
        System.out.println();
    }
}
