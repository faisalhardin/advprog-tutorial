package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class NetworkExpert extends Employees {
    //TODO Implement


    public NetworkExpert(String name, double salary) {
        if (salary < 50000.00) {
            throw new IllegalArgumentException("CTO Salary must not lower than 50000.00");
        }
        super.name = name;
        super.salary = salary;
        super.role = "Network Expert";
    }

    public double getSalary() {
        return salary;
    }
}
