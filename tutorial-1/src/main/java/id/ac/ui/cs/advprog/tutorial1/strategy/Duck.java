package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

     FlyBehavior flyBehavior;
     QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    // TODO Complete me!
    public abstract void display();

    public void swim(){
        System.out.println("Swim");
    }

    public void setFlyBehavior(FlyBehavior newFlyBehavior){
        flyBehavior = newFlyBehavior;
    }

    public void setQuackBehavior(QuackBehavior newQuackBehavior){
        quackBehavior = newQuackBehavior;
    }
}
