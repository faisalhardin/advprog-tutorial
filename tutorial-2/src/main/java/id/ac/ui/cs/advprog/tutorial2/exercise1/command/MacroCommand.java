package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.Collections;

public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        // TODO Complete me!
        this.commands.stream().forEach(command -> command.execute());
    }

    @Override
    public void undo() {
        // TODO Complete me!

        // this.commands.stream().boxed().sorted(Collections.reverseOrder()).forEach(command -> command.undo());
        for (int i = this.commands.size() - 1; i >= 0; i--) {
            this.commands.get(i).undo();
        }

    }
}
