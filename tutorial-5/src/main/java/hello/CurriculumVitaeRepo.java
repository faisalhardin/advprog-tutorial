package hello;

import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Repository;


@Repository
public class CurriculumVitaeRepo {
    private static Map<Integer, CurriculumVitae> cv;

    static {

        cv = new HashMap<Integer, CurriculumVitae>() {

            {
                put(1, new CurriculumVitae(1, "Faisal Hardin", "23 Juni 95", "Jalan Kober",
                        "SD Negeri 1, SMP Negeri 2, SMA Negeri 3",
                        "Tertarik dengan pengembangan web back end"));
            }
        };
    }

    public CurriculumVitae getMyCurriculumVitae() {
        return this.cv.get(1);
    }
}
