package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Component
@Controller
public class GreetingController {


    @Autowired
    private CurriculumVitaeRepo cvRepo;

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = true)
                                       String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }

    @GetMapping("/my-cv")
    public void cv(@RequestParam(name = "name", required = false)
                                 String name, Model model) {
        if (name == null) {
            name = "This is my CV";
        } else {
            name = name + ", I hope you are interested in hiring me";
        }
        model.addAttribute("visitor", name);
        model.addAttribute("cvname", cvRepo.getMyCurriculumVitae().getName());
        model.addAttribute("birthdate", cvRepo.getMyCurriculumVitae().getBirthdate());
        model.addAttribute("address", cvRepo.getMyCurriculumVitae().getAddress());
        model.addAttribute("education", cvRepo.getMyCurriculumVitae().getEducation());
        model.addAttribute("description", cvRepo.getMyCurriculumVitae().getDescription());

    }

}
